+++
date = "2019-07-15"
title = "ES6-变量的解构赋值"
slug = "js-destructuring" 
tags = ['javascript','ES6']
categories = ['Frontend']
+++


#### 描述
根据一定的模式，从数组/对象中提取值，对变量进行赋值。
简单理解为，从一些数据中提取一些值，赋值给另外一些变量。


#### 被提取值的对象
被提取的对象的值可以被遍历出来。如  `数组 Array`、`对象 Object`


#### 被赋值的变量
被赋值的变量，是关于变量的一些集合。如 `数组 Array`、`对象 Object`。`eg: let [a, b] = [ 1, 2]`，等号左边即为变量的集合，此时表现为数组。


#### 如何解构赋值
- 只要等号两边的模式相同，且对象的属性值严格等于 `undefined`， 左边的变量就可以被赋值。如：`let {x} = {x: 1}`
- 解构赋值的规则是，只要等号右边的值不是对象或数组，就先将其转为对象，再解构赋值


下面是一些例子
```js
let [a] = ['a'];  // a:'a'
let [a] = {a: 'a'};  // error 
let [x,y] = [1,2];  // x: 1, y: 2
let {id, name} = {id: 1, name: 'Tom'};  // id: 1, name: Tom
let {id, name} = [1, 'Tom'];  // id: undefined, name: undefined
```

被赋值的对象的数据类型 必须和 被解构的对象的数据类型保持一致。如上面，如果一个对象被解构为数组中的值，则会报错。但是反过来，数组被解构为对象则没有问题。原因是，`js`中，数组是对象的一种。解构赋值时，数组可以转换为对象，从而等号两边的模式相同，进行解构。但是由于内在的结构不同，所以找不到 `id`，`name`对应的值，赋值失败。

-------------------

#### 用法

- 简单的变量赋值：通过数组和对象进行普通的赋值

```js
let [a] = [1];  // 1
let [x, y] = [1, 2, 3] // 1, 2
let [m, , n] = [1, 2, 3] // 1, 3
let {id, name} = {id: 1, name: 'Tom'} // 1, Tom
let {id, age} = {id: 1, name: 'Tom', age: '18'}  // 1, 18
let {student: s} = {student: 'str'}  // s: str
let [head, ...tail] = [1, 2, 3, 4]
// head 1
// tail 2, 3, 4 
```

- 嵌套赋值

```js
let [x, y] = [1, {num: 1}] // 1, {num: 1}
let {name, type} = {'fruit', type: ['watermelon', 'apple']}
// name fruit
// type   ['watermelon', 'apple']
```

- 其他解构赋值

```js
// 字符串
const [a, b] = '你好';
// a 你
// b 好

let {length: len} = '你好';
// len 2
```

#### 用途
- 函数参数定义

```js
function Fn ([x,y]) {}  
// Fn([1,2]);
function Fn ({x, y}) {} 
// Fn({y: 2, x: 1});
function Fn({id = 1, name}) {} 
// Fn({name: 'Tom'})
```

- 函数返回多个值

```js
function Fn() {  
    return {title: '测试', date: '2019-07-15'};
}
let {date} = Fn(); // 2019-07-15
```

- 遍历`Map`

```js
const demoMap = new Map();
demoMap.set('operate', 'create');
demoMap.set('type', 'map');
for (let [key, val] of demoMap) {
console.log(`${key} is : ${val}`);
// operate is : create
// type is : map
}
```

- 交换值

```js
let x = 1;
let y = 2;
[x, y] = [y, x];
```

