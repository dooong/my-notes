+++
date = "2019-07-24"
title = "ES6-Map结构"
slug = "js-map"
tags = ['javascript','ES6']
categories = ['Frontend']
+++

### `Map`
一种对象结构，键值对集合。键只能是字符串

#### 属性
- `size`：`Map` 对象的成员个数


#### 方法
- `set(set, val)`：设置一个key的值
- `get(key)`：获取一个key的值
- `delete(key)`：删除一个key的值
- `clear()`：清空 `Map` 对象
- `has(key)`：判断是否含有某个key的值，返回 `Boolean`
- `keys()`：获取对象的所有 key，返回一个迭代对象
- `values()`：获取对象的所有 value，返回一个迭代对象
- `entires()`：返回一个新的迭代对象，对象的每个元素为 `[key, val]`
- `forEach()`

----------

### `WeakMap`

键值对集合，键只能是对象（键是弱引用的）
#### 方法
- `set(set, val)`
- `get(key)`
- `has(key)`
- `delete(key)`

