+++
date = "2019-06-29"
title = "JS 调试技巧"
slug = "js-debugger" 
tags = ['javascript']
categories = ['Frontend']
+++

#### debugger
```js
// 条件判断
if (thisThing) {
    debugger; 
}
```

#### console.table
```js
// 表格显示对象
var animals = [
    { animal: 'Horse', name: 'Henry', age: 43 },
    { animal: 'Dog', name: 'Fred', age: 13 },
    { animal: 'Cat', name: 'Frodo', age: 18 }
];

console.table(animals);
```

#### 测试循环
```js
console.time('Timer1');

var items = [];

for(var i = 0; i < 100000; i++){
   items.push({index: i});
}

console.timeEnd('Timer1');
```

#### console.trace
```js
// 获取函数的堆栈跟踪信息
var car;
var func1 = function() {
  func2();
}

var func2 = function() {
  func4();
}
var func3 = function() {
}

var func4 = function() {
  car = new Car();
  car.funcX();
}
var Car = function() {
  this.brand = ‘volvo’;
  this.color = ‘red’;
  this.funcX = function() {
    this.funcY();
  }

  this.funcY = function() {
    this.funcZ();
  }

  this.funcZ = function() {
    console.trace(‘trace car’)
  }
}
func1();
```
#### 寻找重点
```js
console.log, console.debug, console.warn, console.info, console.error
```