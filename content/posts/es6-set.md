+++
date = "2019-07-23"
title = "ES6-Set结构"
slug = "js-set"
tags = ['javascript','ES6']
categories = ['Frontend']
+++

### `Set`
类似数组，成员值唯一（`Object`、`NaN`）除外。`Set` 对象没有键名，只有值。`Set`本身是一个对象，包含一些属性、方法以及存储的值。

#### 属性
- `size`：`Set`对象值的个数

#### 方法
- `add(val)`：添加一个值到 `Set` 对象(添加在尾部)，返回 `Set` 对象
- `delete(val)`：删除 `Set` 对象的一个值，返回执行 `delete` 操作前的 `set.has(val)`的值
- `clear`：移除 `Set` 对象内的所有值
- `has(val)`：判断某个值是否在对象中，返回 `Boolean`
- `keys()`：以一个迭代对象的形式，返回 `Set` 的所有值
- `values()`：以一个迭代对象的形式，返回 `Set` 的所有值
- `entries()`：以一个迭代对象的形式，返回 `Set` 所有的键值对，形如 `[value, value]`
- `forEach()`：类似 `Array` 的 `forEach`

#### 用途 `demo`
```js
const numSet1 = new Set([1, 2, 3]);
const numSet2 = new Set([2, 3, 4]);
// 并集
const union = new Set([...numSet1, ...numSet2]); // {1, 2, 3, 4}
// 交集
const intersect = new Set([...numSet1].filter(val => numSet2.has(val))); // {2, 3}
// 差集
const difference = new Set([...numSet1].filter(val => !numSet2.has(val))); // {1}
```
----------

### `WeakSet`
存储对象的引用的一个集合，与 `Set` 类似。但是，`WeakSet` 的成员只能是对象。

#### 与 `Set` 的区别

- 只能存储对象的引用，不能存储值

```js
const arr = [1, 2, 3];
const myWeakSet = new WeakSet(arr);
// 会报错： Uncaught TypeError: Invalid value used in weak set(…)
// 因为 WeakSet 的成员必须为对象。上面的方式，会使得 arr 的每个元素成为 WeakSet 的对象，这是不合要求的
```
- 无法被遍历

```js
const arr = [1, 2, 3];
const mySet = new Set(arr);
const myWeakSet = new WeakSet().add(arr);
arr[0] = 0;
console.log(arr); // [0, 2, 3]
console.log(mySet); // {1, 2, 3}
console.log(myWeakSet); // [[1, 2, 3]]
```

#### 方法
- `add(val)`
- `delete(val)`
- `has(val)`