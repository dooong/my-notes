+++
date = "2019-07-10"
title = "Date对象(JS) 常用操作"
slug = "js-date" 
tags = ['javascript']
categories = ['Frontend']
+++

#### 实例化对象的4种方式
```js
// 无参数 
new Date(); // 默认此时的时间和日期

// 1个参数
new Date(unixTimeStamp); // UNIX时间戳
new Date(dateString); // 表示日期时间的字符串

// 多个参数
new Date(year, monthIndex, day, hours, mintues, seconds, milliseconds);
```
- `year`为年份的整数值，0-99会映射为1900-1999
- `day`从 **1** 开始，其他默认为 **0**

#### 获取当前日期时间
```js
function getDate(format) {
    const checkNum = (val) => {
        if (val < 10) {
            return '0' + val;
        } else {
            return val;
        }
    };
    const nowDate = new Date();
    const yy = nowDate.getFullYear();
    const month = nowDate.getMonth();
    const mm = month + 1 > 12 ? 12 : checkNum(month+1);
    const dd = nowDate.getDate();
    const hh = nowDate.getHours();
    const min = checkNum(nowDate.getMinutes());
    const ss = checkNum(nowDate.getSeconds());

    let dateStr = `${yy}-${mm}-${dd} ${hh}:${min}:${ss}`;
    let result = '';
    switch (format) {
        case 'yyyy-MM-dd': result = dateStr.split(' ')[0]; break;
        case 'HH-mm-ss': result = dateStr.split(' ')[1]; break;
        default: result = dateStr;
    }
    return result;

}
console.log(getDate('yyyy-MM-dd')); // 2019-07-11
console.log(getDate('HH-mm-ss')); // 20:20:02
console.log(getDate()); // 2019-07-11 20:20:02
```

#### 根据时间字符串，获取时间戳
先实例化 `Date`对象，再调用 `getTime()`即可。
```
new Date('2019-07-11 20:20:02'); // yyyy-MM-dd HH-mm-ss
new Date('2019','06','11', '20', '20', '7'); // 月份从0开始，可以是双位数或单位数
new Date("July 11,2019");
new Date("July 11,2019 20:20:02");
```